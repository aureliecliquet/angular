
  app.controller('PostsCtrl', function ($scope, $rootScope, Post) {
    $rootScope.loading = true;
    Post.find()
      .then(
        function(posts) {
          $rootScope.loading = false;
          $scope.posts = posts;
          console.log($scope.posts);
        },
        function(msg){
          alert(msg);
        }
      )
  })
